from sqlalchemy.dialects.postgresql import ENUM


class ChoiceEnum(ENUM):
    """Adds ability to use python enums in Postgresql."""

    def __init__(self, *enums, **kw):
        kw.update({"values_callable": lambda obj: [e.value for e in obj]})
        super(ChoiceEnum, self).__init__(*enums, **kw)
