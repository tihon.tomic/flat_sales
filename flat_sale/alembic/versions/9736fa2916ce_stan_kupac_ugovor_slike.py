"""Stan,Kupac,Ugovor,Slike

Revision ID: 9736fa2916ce
Revises: da6f2da8a477
Create Date: 2021-11-17 21:37:24.046829

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
import flat_sale

revision = '9736fa2916ce'
down_revision = 'da6f2da8a477'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('kupac',
    sa.Column('tip_kupca', flat_sale.types.ChoiceEnum('Fizicko Lice', 'Pravno Lice', name='tipkupca'), nullable=False),
    sa.Column('naziv_firme_ili_ime_kupca', sa.String(), nullable=False),
    sa.Column('email', sa.String(), nullable=True),
    sa.Column('broj_telefona', sa.String(), nullable=True),
    sa.Column('pib_jmbg', sa.String(), nullable=False),
    sa.Column('adresa', sa.String(), nullable=True),
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('datum_azuriranja', sa.DateTime(), nullable=True),
    sa.Column('datum_kreiranja', sa.DateTime(), nullable=False),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('pib_jmbg')
    )
    op.create_table('stan',
    sa.Column('kupac_id', sa.Integer(), nullable=True),
    sa.Column('lamela', sa.String(), nullable=True),
    sa.Column('kvadratura', sa.String(), nullable=True),
    sa.Column('sprat', sa.Integer(), nullable=True),
    sa.Column('broj_soba', sa.Integer(), nullable=True),
    sa.Column('orijentisanost', sa.String(), nullable=True),
    sa.Column('broj_terasa', sa.Integer(), nullable=True),
    sa.Column('cena', sa.Numeric(precision=20, scale=2), nullable=False),
    sa.Column('status', flat_sale.types.ChoiceEnum('Dostupan', 'Rezervisan', 'Prodat', name='statusstana'), nullable=False),
    sa.Column('adresa', sa.String(), nullable=True),
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('datum_azuriranja', sa.DateTime(), nullable=True),
    sa.Column('datum_kreiranja', sa.DateTime(), nullable=False),
    sa.ForeignKeyConstraint(['kupac_id'], ['kupac.id'], name='fk_stan_kupac_id'),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('stan_file',
    sa.Column('stan_id', sa.Integer(), nullable=False),
    sa.Column('file_path', sa.String(), nullable=True),
    sa.Column('filename', sa.String(), nullable=True),
    sa.Column('deleted', sa.Boolean(), nullable=True),
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('datum_azuriranja', sa.DateTime(), nullable=True),
    sa.Column('datum_kreiranja', sa.DateTime(), nullable=False),
    sa.ForeignKeyConstraint(['stan_id'], ['stan.id'], name='fk_stanfile_stan_id'),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('ugovor',
    sa.Column('odobreno', flat_sale.types.ChoiceEnum('Da', 'Ne', name='odobrenje'), nullable=False),
    sa.Column('dogovorena_cena', sa.Integer(), nullable=True),
    sa.Column('nacin_placanja', sa.String(), nullable=True),
    sa.Column('broj_ugovora', sa.String(), nullable=True),
    sa.Column('datum_ugovora', sa.Date(), nullable=True),
    sa.Column('odobrio_korisnik', sa.Integer(), nullable=True),
    sa.Column('napomena', sa.String(), nullable=True),
    sa.Column('datum_prve_posete', sa.Date(), nullable=True),
    sa.Column('kupac_id', sa.Integer(), nullable=False),
    sa.Column('stan_id', sa.Integer(), nullable=False),
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('datum_azuriranja', sa.DateTime(), nullable=True),
    sa.Column('datum_kreiranja', sa.DateTime(), nullable=False),
    sa.ForeignKeyConstraint(['kupac_id'], ['kupac.id'], name='fk_kupac_id'),
    sa.ForeignKeyConstraint(['odobrio_korisnik'], ['korisnik.id'], name='fk_ugovor_korisnik_id'),
    sa.ForeignKeyConstraint(['stan_id'], ['stan.id'], name='fk_stan_id'),
    sa.PrimaryKeyConstraint('id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('ugovor')
    op.drop_table('stan_file')
    op.drop_table('stan')
    op.drop_table('kupac')
    # ### end Alembic commands ###
