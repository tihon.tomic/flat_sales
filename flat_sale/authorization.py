import os
from typing import List

from fastapi import Depends, HTTPException
from fastapi.security import OAuth2PasswordBearer
from jose import jwt
from passlib.context import CryptContext
from datetime import datetime, timedelta

# ----------------------
# Security configuration
# ----------------------
from starlette import status

from flat_sale.crud import get_user_by_korisnicko_ime
from flat_sale.models import Korisnik
from flat_sale.utils import verify_password

JWT_SECRET_KEY: str = os.getenv(
    "JWT_SECRET_KEY", "6dc6650063888cd09d0554627d2a50bb37ce941c0acf7fb8b05bc917d343a35a"
)
JWT_ALGORITHM: str = "HS512"
JWT_ACCESS_TOKEN_EXPIRE_MINUTES: int = 720

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="login")


async def authenticate_user(username: str, password: str, ):
    user = await get_user_by_korisnicko_ime(korisnicko_ime=username)
    if not user:
        return False
    if not verify_password(password, user.sifra):
        return False
    return user


def create_access_token(data: dict) -> str:
    """Creates JWT access token with given data that will be encoded into the token."""
    to_encode = data.copy()
    expire = datetime.now() + timedelta(minutes=JWT_ACCESS_TOKEN_EXPIRE_MINUTES)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, JWT_SECRET_KEY, algorithm=JWT_ALGORITHM)
    return encoded_jwt


async def get_current_user(token: str = Depends(oauth2_scheme)):
    try:
        payload = jwt.decode(token, JWT_SECRET_KEY, JWT_ALGORITHM)
        user = await get_user_by_korisnicko_ime(korisnicko_ime=payload.get('korisnicko_ime'))
    except:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Losi kredencijali."
        )
    return user


# role based authorization

class RoleChecker:
    def __init__(self, allowed_roles: List):
        self.allowed_roles = allowed_roles

    def __call__(self, korisnik: Korisnik = Depends(get_current_user)):
        if korisnik.rola.value not in self.allowed_roles:
            raise HTTPException(status_code=403, detail="")
