from datetime import datetime

from sqlalchemy.dialects.postgresql import ARRAY

from flat_sale import db
from flat_sale.choices import KorisnikRoles, TipKupca, StatusStana, Odobrenje
from flat_sale.types import ChoiceEnum


class BaseModel(db.Model):
    __abstract__ = True

    id = db.Column(db.Integer(), primary_key=True)

    datum_azuriranja = db.Column(db.DateTime(), nullable=True)
    datum_kreiranja = db.Column(db.DateTime(), default=datetime.now, nullable=False)


class BaseAuthorizationMixin(BaseModel):
    """Abstract table for all tables that should have user authorization."""

    __abstract__ = True

    korisnicko_ime = db.Column(db.String(length=50), nullable=False, unique=True)
    sifra = db.Column(db.String(length=225), nullable=True)


class Korisnik(BaseAuthorizationMixin):
    """Users that are part of the administration."""

    __tablename__ = "korisnik"

    ime = db.Column(db.String(), nullable=True)
    prezime = db.Column(db.String(), nullable=True)
    rola = db.Column(ChoiceEnum(KorisnikRoles), nullable=False)

    @staticmethod
    def representation():
        return "Korisnik"


class Kupac(BaseModel):
    __tablename__ = "kupac"

    tip_kupca = db.Column(ChoiceEnum(TipKupca), nullable=False)
    naziv_firme_ili_ime_kupca = db.Column(db.String(), nullable=False)
    email = db.Column(db.String(), nullable=True)
    broj_telefona = db.Column(db.String(), nullable=True)
    pib_jmbg = db.Column(db.String(), nullable=False, unique=True)
    adresa = db.Column(db.String(), nullable=True)


class Stan(BaseModel):
    __tablename__ = "stan"

    kupac_id = db.Column(db.Integer(), db.ForeignKey("kupac.id", name="fk_stan_kupac_id"),
                         nullable=True)
    lamela = db.Column(db.String(), nullable=True)
    kvadratura = db.Column(db.String(), nullable=True)
    sprat = db.Column(db.Integer(), nullable=True)
    broj_soba = db.Column(db.Integer(), nullable=True)
    orijentisanost = db.Column(db.String(), nullable=True)
    broj_terasa = db.Column(db.Integer(), nullable=True)
    cena = db.Column(db.Numeric(precision=20, scale=2), nullable=False)
    status = db.Column(ChoiceEnum(StatusStana), default=StatusStana.DOSTUPAN.value, nullable=False)
    adresa = db.Column(db.String(), nullable=True)

    def __init__(self, **kw):
        super().__init__(**kw)
        self._files = set()
        self._kupci = set()

    @property
    def kupci(self):
        return self._kupci

    @kupci.setter
    def kupci(self, value):
        if value.id:
            self._kupci.add(value)

    @property
    def files(self):
        return self._files

    @files.setter
    def files(self, value):
        if value.id:
            self._files.add(value)


class StanFile(BaseModel):
    __tablename__ = "stan_file"

    stan_id = db.Column(db.Integer(), db.ForeignKey("stan.id", name="fk_stanfile_stan_id"),
                        nullable=False)
    file_path = db.Column(db.String())
    filename = db.Column(db.String())
    deleted = db.Column(db.Boolean(), default=False)


class KupacMixin(BaseModel):
    """Abstract table that has reference to `Kupac` in database."""
    __abstract__ = True

    kupac_id = db.Column(db.Integer, db.ForeignKey("kupac.id", name="fk_kupac_id"), nullable=False)

    _kupac = None

    @property
    def kupac(self):
        return self._kupac

    @kupac.setter
    def kupac(self, value):
        # If value id exists, bind object to property.
        if value.id:
            self._kupac = value


class StanMixin(BaseModel):
    """Abstract table that has reference to `Stan` in database."""
    __abstract__ = True

    stan_id = db.Column(db.Integer, db.ForeignKey("stan.id", name="fk_stan_id"), nullable=False)

    _stan = None

    @property
    def stan(self):
        return self._stan

    @stan.setter
    def stan(self, value):
        # If value id exists, bind object to property.
        if value.id:
            self._stan = value


class Ugovor(KupacMixin, StanMixin, BaseModel):
    __tablename__ = "ugovor"

    odobreno = db.Column(ChoiceEnum(Odobrenje), nullable=False, default=Odobrenje.DA.value)
    dogovorena_cena = db.Column(db.Integer(), nullable=True)
    nacin_placanja = db.Column(db.String(), nullable=True)
    broj_ugovora = db.Column(db.String(), nullable=True)
    datum_ugovora = db.Column(db.Date(), nullable=True)
    odobrio_korisnik = db.Column(db.Integer(),
                                 db.ForeignKey("korisnik.id", name="fk_ugovor_korisnik_id"),
                                 nullable=True)
    napomena = db.Column(db.String(), nullable=True)
    datum_prve_posete = db.Column(db.Date(), nullable=True)
    word_doc_path = db.Column(db.String(), nullable=True)

    def __init__(self, **kw):
        super().__init__(**kw)
        self._stan = set()
        self._kupac = set()
        self._odobrile_finansije = set()

    @property
    def kupac(self):
        return self._kupac

    @kupac.setter
    def kupac(self, value):
        if value.id:
            self._kupac.add(value)

    @property
    def stan(self):
        return self._stan

    @stan.setter
    def stan(self, value):
        if value.id:
            self._stan.add(value)

    @property
    def odobrile_finansije(self):
        return self._odobrile_finansije

    @odobrile_finansije.setter
    def odobrile_finansije(self, value):
        if value.id:
            self._odobrile_finansije.add(value)
