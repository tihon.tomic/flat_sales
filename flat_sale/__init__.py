from gino.ext.starlette import Gino
from pydantic import PostgresDsn

SQLALCHEMY_DATABASE_URL = "postgresql+psycopg2://postgres:qwerty@localhost/flat"

DATABASE_URI = PostgresDsn.build(
        scheme="postgresql",
        host="localhost",
        port="5432",
        path="/flat",
        user="postgres",
        password="qwerty",
    )

db = Gino(dsn=DATABASE_URI)

