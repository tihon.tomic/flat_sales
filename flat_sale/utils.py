from secrets import token_hex

from docxtpl import DocxTemplate
from fastapi import HTTPException
from passlib.context import CryptContext
from starlette import status

from flat_sale.models import Kupac, Stan

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")


def get_password_hash(password):
    return pwd_context.hash(password)


def verify_password(plain_password, hashed_password):
    return pwd_context.verify(plain_password, hashed_password)


async def generate_ugovor_data(ugovor):
    kupac = await Kupac.get(ugovor.kupac_id)
    stan = await Stan.get(ugovor.stan_id)
    cena = ugovor.dogovorena_cena if ugovor.dogovorena_cena else stan.cena
    temp_data = {"kupac": kupac.naziv_firme_ili_ime_kupca,
                 "cena": cena}

    doc = DocxTemplate("/home/tihon/workplace/flat-sale/static/templates/ugovor_template.docx")
    doc.render(temp_data)
    filepath = f"/home/tihon/workplace/flat-sale/static/stanovi/ugovor_{token_hex(4)}.docx"
    doc.save(filepath)

    return filepath


def check_file_type(extension):
    if not extension == 'jpg':
        raise HTTPException(status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
                            detail="Slike moraju biti u jpg formatu.")
