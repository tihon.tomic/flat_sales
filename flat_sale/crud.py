from flat_sale.models import Korisnik, Kupac


async def get_user(korisnik_id: int):
    return await Korisnik.get(korisnik_id)


async def get_user_by_korisnicko_ime(korisnicko_ime: str):
    return await Korisnik.query.where(Korisnik.korisnicko_ime == korisnicko_ime).gino.first()


async def get_kupac(kupac_id: int):
    return await Kupac.get(kupac_id)


async def get_kupac_by_pibjmbg(pibjmbg: str):
    return await Kupac.query.where(Kupac.pib_jmbg == pibjmbg).gino.first()
