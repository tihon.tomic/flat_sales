from enum import Enum


class KorisnikRoles(Enum):
    ADMINISTRATOR = "Administrator"
    PRODAVAC = "Prodavac"
    FINANSIJE = "Finansije"


class TipKupca(Enum):
    FIZICKO = "Fizicko Lice"
    PRAVNO = "Pravno Lice"


class StatusStana(Enum):
    DOSTUPAN = "Dostupan"
    REZERVISAN = "Rezervisan"
    PRODAT = "Prodat"


class Odobrenje(Enum):
    DA = "Da"
    NE = "Ne"
