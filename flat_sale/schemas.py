from datetime import datetime, date
from decimal import Decimal
from typing import Optional, List

from pydantic import BaseModel, constr, validator

from flat_sale.choices import KorisnikRoles, TipKupca, StatusStana, Odobrenje
from flat_sale.utils import get_password_hash


class KorisnikSchema(BaseModel):
    id: int
    datum_kreiranja: Optional[datetime]
    datum_azuriranja: Optional[datetime]
    ime: Optional[str]
    prezime: Optional[str]
    rola: KorisnikRoles
    korisnicko_ime: str

    class Config:
        orm_mode = True


class CreateKorisnikSchema(BaseModel):
    korisnicko_ime: str
    sifra: constr(min_length=5)
    ime: Optional[str]
    prezime: Optional[str]
    rola: KorisnikRoles

    _hash_sifra = validator("sifra", pre=False, allow_reuse=True)(get_password_hash)


class UpdateKorisnikSchema(BaseModel):
    korisnicko_ime: Optional[str] = None
    sifra: Optional[constr(min_length=5)] = None
    ime: Optional[str] = None
    prezime: Optional[str] = None

    _hash_sifra = validator("sifra", pre=False, allow_reuse=True)(get_password_hash)


class KupacSchema(BaseModel):
    id: int
    datum_kreiranja: Optional[datetime]
    datum_azuriranja: Optional[datetime]
    tip_kupca: TipKupca
    naziv_firme_ili_ime_kupca: str
    email: Optional[str]
    broj_telefona: Optional[str]
    pib_jmbg: str
    adresa: Optional[str]

    class Config:
        orm_mode = True


class CreateKupacSchema(BaseModel):
    naziv_firme_ili_ime_kupca: str
    tip_kupca: TipKupca
    pib_jmbg: str
    email: Optional[str]
    broj_telefona: Optional[str]
    adresa: Optional[str]


class UpdateKupacSchema(BaseModel):
    naziv_firme_ili_ime_kupca: Optional[str]
    tip_kupca: Optional[TipKupca]
    pib_jmbg: Optional[str]
    email: Optional[str]
    broj_telefona: Optional[str]
    adresa: Optional[str]


class StanFileSchema(BaseModel):
    id: int
    datum_kreiranja: Optional[datetime]
    datum_azuriranja: Optional[datetime]
    stan_id: int
    file_path: str
    filename: str
    deleted: bool

    class Config:
        orm_mode = True


class StanSlikeSchema(BaseModel):
    file_path: str
    filename: str
    deleted: bool

    class Config:
        orm_mode = True


class StanKupacSchema(BaseModel):
    id: int
    tip_kupca: TipKupca
    naziv_firme_ili_ime_kupca: str
    email: Optional[str]
    broj_telefona: Optional[str]
    pib_jmbg: str
    adresa: Optional[str]
    napomena: Optional[str]
    datum_prve_posete: Optional[datetime]
    broj_ugovora: Optional[str]
    datum_ugovora: Optional[datetime]

    class Config:
        orm_mode = True


class StanBaseSchema(BaseModel):
    id: int
    datum_kreiranja: Optional[datetime]
    datum_azuriranja: Optional[datetime]
    kupac_id: Optional[int]
    lamela: Optional[str]
    kvadratura: Optional[str]
    sprat: Optional[int]
    broj_soba: Optional[int]
    orijentisanost: Optional[str]
    broj_terasa: Optional[int]
    cena: Optional[Decimal]
    status: Optional[StatusStana]
    adresa: Optional[str]

    class Config:
        orm_mode = True


class StanFilesSchema(StanBaseSchema):
    files: Optional[List[StanSlikeSchema]]

    class Config:
        orm_mode = True


class StanKupciSchema(StanBaseSchema):
    kupci: Optional[List[StanKupacSchema]]

    class Config:
        orm_mode = True


class CreateStanSchema(BaseModel):
    lamela: Optional[str]
    kvadratura: Optional[str]
    sprat: Optional[int]
    broj_soba: Optional[int]
    orijentisanost: Optional[str]
    broj_terasa: Optional[int]
    cena: Optional[Decimal]
    status: Optional[StatusStana]
    adresa: Optional[str]


class UpdateStanSchema(BaseModel):
    kupac_id: Optional[int]
    lamela: Optional[str]
    kvadratura: Optional[str]
    sprat: Optional[int]
    broj_soba: Optional[int]
    orijentisanost: Optional[str]
    broj_terasa: Optional[int]
    cena: Optional[Decimal]
    status: Optional[StatusStana]
    adresa: Optional[str]


class UgovorBaseSchema(BaseModel):
    id: int
    datum_kreiranja: Optional[datetime]
    datum_azuriranja: Optional[datetime]
    kupac_id: int
    stan_id: int
    odobreno: Optional[Odobrenje]
    odobrio_korisnik: Optional[int]
    dogovorena_cena: Optional[int]
    nacin_placanja: Optional[str]
    broj_ugovora: Optional[str]
    datum_ugovora: Optional[date]
    napomena: Optional[str]
    datum_prve_posete: Optional[date]

    class Config:
        orm_mode = True


class CreateUgovorSchema(BaseModel):
    kupac_id: int
    stan_id: int
    odobreno: Optional[Odobrenje]
    dogovorena_cena: Optional[int]
    nacin_placanja: Optional[str]
    napomena: Optional[str]
    datum_prve_posete: Optional[date]

    class Config:
        orm_mode = True


class UgovorOdobrenjeSchema(UgovorBaseSchema):
    kupac: Optional[List[KupacSchema]]
    stan: Optional[List[StanBaseSchema]]

    class Config:
        orm_mode = True


class UgovorUpdateReturnSchema(UgovorOdobrenjeSchema):
    odobrile_finansije: Optional[List[KorisnikSchema]]

    class Config:
        orm_mode = True


class UgovorOdobrenjeUpdateSchema(BaseModel):
    odobreno: Optional[Odobrenje]
    dogovorena_cena: Optional[int]
    nacin_placanja: Optional[str]
    napomena: Optional[str]
    datum_prve_posete: Optional[date]


class StanStatusSchema(BaseModel):
    status: StatusStana
    kupac_id: int
    broj_ugovora: Optional[str]


class StanRezervacijaSchema(BaseModel):
    kupac_id: int


class StanProdajaSchema(BaseModel):
    broj_ugovora: str


class IzvestajKupacSchema(UgovorBaseSchema):
    stan: Optional[List[StanBaseSchema]]

    class Config:
        orm_mode = True
