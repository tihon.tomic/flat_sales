from datetime import datetime, date
from secrets import token_hex
from typing import List

import aiofiles
from fastapi import Depends, FastAPI, HTTPException, Path, File, UploadFile
from fastapi.security import OAuth2PasswordRequestForm
from fastapi_pagination import paginate, Page, Params
from sqlalchemy import and_
from starlette import status

import crud, schemas
from flat_sale import db
from flat_sale.authorization import verify_password, authenticate_user, create_access_token, \
    get_current_user, RoleChecker

from flat_sale.choices import KorisnikRoles, Odobrenje, StatusStana
from flat_sale.models import Korisnik, Kupac, Stan, StanFile, Ugovor
from flat_sale.utils import generate_ugovor_data, check_file_type

app = FastAPI()
db.init_app(app)


@app.post("/users/", response_model=schemas.KorisnikSchema,
          dependencies=[Depends(RoleChecker(["Adrministrator"]))])
async def create_user(korisnik: schemas.CreateKorisnikSchema,
                      current_korisnik=Depends(get_current_user)
                      ):
    if current_korisnik.rola.value != KorisnikRoles.ADMINISTRATOR.value:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Samo Admin moze da kreira nove Korisnike."
        )
    user = await crud.get_user_by_korisnicko_ime(korisnicko_ime=korisnik.korisnicko_ime)
    if user:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Korisnik vec postoji.")

    return await Korisnik.create(**korisnik.dict(exclude_unset=True))


@app.get("/users/", response_model=Page[schemas.KorisnikSchema],
         dependencies=[Depends(RoleChecker(["Administrator"]))])
async def read_users(current_korisnik=Depends(get_current_user),
                     params: Params = Depends()):
    if current_korisnik.rola.value != KorisnikRoles.ADMINISTRATOR.value:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Zabranje pristup."
        )
    users = await Korisnik.query.gino.all()
    return paginate(users, params)


@app.get("/users/{korisnik_id}", response_model=schemas.KorisnikSchema,
         dependencies=[Depends(RoleChecker(["Administrator"]))])
async def read_user(korisnik_id: int = Path(..., gt=0),
                    current_korisnik=Depends(get_current_user)):
    if current_korisnik.id != korisnik_id and current_korisnik.rola.value != KorisnikRoles.ADMINISTRATOR.value:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Zabranjen pristup."
        )
    user = await crud.get_user(korisnik_id=korisnik_id)
    if user:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail="Korisnik nije pronadjen.")
    return user


@app.patch("/users/{korisnik_id}", response_model=schemas.KorisnikSchema,
           dependencies=[Depends(RoleChecker(["Administrator"]))])
async def update_user(update_data: schemas.UpdateKorisnikSchema, korisnik_id: int = Path(..., gt=0),
                      current_korisnik=Depends(get_current_user)):
    if current_korisnik.id != korisnik_id and current_korisnik.rola.value != KorisnikRoles.ADMINISTRATOR.value:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Zabranjen pristup."
        )

    user = await crud.get_user(korisnik_id=korisnik_id)
    if not user:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail="Korisnik nije pronadjen.")
    updated_user = await user.update(**update_data.dict(exclude_unset=True),
                                     datum_azuriranja=datetime.now()).apply()

    return updated_user._instance


@app.post("/login/")
async def generate_token(form_data: OAuth2PasswordRequestForm = Depends()):
    user = await authenticate_user(form_data.username, form_data.password)

    if not user or not verify_password(form_data.password, user.sifra):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Losi kredencijali."
        )
    access_token = create_access_token(
        data={"korisnicko_ime": user.korisnicko_ime, "rola": user.rola.value})
    orm_user = schemas.KorisnikSchema.from_orm(user)

    return {"access_token": access_token, "token_type": "Bearer", "user": orm_user}


@app.get("/kupci/", response_model=Page[schemas.KupacSchema],
         dependencies=[Depends(RoleChecker(["Adrministrator", "Prodavac"]))])
async def get_kupci(current_korisnik=Depends(get_current_user), params: Params = Depends()):
    kupci = await Kupac.query.gino.all()
    return paginate(kupci, params)


@app.post("/kupac/", response_model=schemas.KupacSchema,
          dependencies=[Depends(RoleChecker(["Adrministrator", "Prodavac"]))])
async def create_kupac(kupac: schemas.CreateKupacSchema,
                       current_korisnik=Depends(get_current_user)):
    kupac_exists = await crud.get_kupac_by_pibjmbg(pibjmbg=kupac.pib_jmbg)
    if kupac_exists:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Kupac vec postoji.")

    return await Kupac.create(**kupac.dict(exclude_unset=True))


@app.patch("/kupac/{kupac_id}", response_model=schemas.KupacSchema,
           dependencies=[Depends(RoleChecker(["Adrministrator", "Prodavac"]))])
async def update_kupac(update_data: schemas.UpdateKupacSchema, kupac_id: int = Path(..., gt=0),
                       current_korisnik=Depends(get_current_user)):
    kupac = await Kupac.get(kupac_id)
    if not kupac:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail="Korisnik nije pronadjen.")
    updated_kupac = await kupac.update(**update_data.dict(exclude_unset=True),
                                       datum_azuriranja=datetime.now()).apply()

    return updated_kupac._instance


@app.get("/stanovi/", response_model=Page[schemas.StanFilesSchema],
         dependencies=[Depends(RoleChecker(["Adrministrator", "Prodavac"]))])
async def get_stanovi(current_korisnik=Depends(get_current_user), params: Params = Depends()):
    stanovi = await Stan.outerjoin(StanFile, Stan.id == StanFile.stan_id).select().gino.load(
        Stan.distinct(Stan.id).load(files=StanFile)).all()
    return paginate(stanovi, params)


@app.post("/stan/", response_model=schemas.StanBaseSchema,
          dependencies=[Depends(RoleChecker(["Adrministrator", "Prodavac"]))])
async def new_stan(stan: schemas.CreateStanSchema, current_korisnik=Depends(get_current_user)):
    return await Stan.create(**stan.dict(exclude_unset=True))


@app.patch("/stan/{stan_id}", response_model=schemas.StanBaseSchema,
           dependencies=[Depends(RoleChecker(["Adrministrator", "Prodavac"]))])
async def update_stan(update_data: schemas.UpdateStanSchema, stan_id: int = Path(..., gt=0),
                      current_korisnik=Depends(get_current_user)):
    stan = await Stan.get(stan_id)
    if not stan:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Stan nije pronadjen.")
    updated_stan = await stan.update(**update_data.dict(exclude_unset=True),
                                     datum_azuriranja=datetime.now()).apply()

    return updated_stan._instance


@app.get("/stan/{stan_id}/slike", response_model=Page[schemas.StanFileSchema],
         status_code=status.HTTP_200_OK,
         dependencies=[Depends(RoleChecker(["Adrministrator", "Prodavac"]))])
async def get_all_photos(
        stan_id: int = Path(..., gt=0),
        current_korisnik=Depends(get_current_user),
        params: Params = Depends()
):
    slike = await StanFile.query.where(
        and_(StanFile.stan_id == stan_id, StanFile.deleted.is_(False))).gino.all()
    return paginate(slike, params)


@app.post("/stan/{stan_id}/slike", response_model=Page[schemas.StanFileSchema],
          status_code=status.HTTP_201_CREATED,
          dependencies=[Depends(RoleChecker(["Adrministrator", "Prodavac"]))])
async def upload_slike(
        file: UploadFile = File(...),
        stan_id: int = Path(..., gt=0),
        current_korisnik=Depends(get_current_user),
        params: Params = Depends()):
    filename, extension = file.filename.rsplit(".", 1)
    check_file_type(extension)
    filename = f"{filename}_{token_hex(4)}.jpg"
    file_path = f"/home/tihon/workplace/flat-sale/static/stanovi/{filename}"

    await StanFile.create(stan_id=stan_id, file_path=file_path, filename=filename)

    file_content = await file.read()
    async with aiofiles.open(file_path, mode="wb") as f:
        await f.write(file_content)

    uploaded_photos = await StanFile.query.where(StanFile.stan_id == stan_id).gino.all()
    return paginate(uploaded_photos, params)


@app.delete("/stan/{slika_id}/", status_code=status.HTTP_204_NO_CONTENT,
            dependencies=[Depends(RoleChecker(["Adrministrator", "Prodavac"]))])
async def delete_slika(
        slika_id: int = Path(..., gt=0),
        current_korisnik=Depends(get_current_user)):
    file = await StanFile.get(slika_id)
    await file.update(deleted=True, datum_azuriranja=datetime.now()).apply()


@app.get("/stan/zainteresovani_kupci", response_model=Page[schemas.StanKupciSchema],
         dependencies=[Depends(RoleChecker(["Adrministrator", "Prodavac"]))])
async def get_zainteresovani_kupci(current_korisnik=Depends(get_current_user),
                                   params: Params = Depends()):
    zainteresovani_kupci = await (Stan.outerjoin(Ugovor, Stan.id == Ugovor.stan_id)
                                  .outerjoin(Kupac, Ugovor.kupac_id == Kupac.id)
                                  .select().where(Stan.status != StatusStana.PRODAT).gino.load(
        Stan.distinct(Stan.id).load(kupci=Kupac)).all())
    return paginate(zainteresovani_kupci, params)


@app.get("/stan/{stan_id}/zainteresovani_kupci", response_model=Page[schemas.StanKupciSchema],
         dependencies=[Depends(RoleChecker(["Adrministrator", "Prodavac"]))])
async def get_zainteresovani_kupci_za_stan(stan_id: int = Path(..., gt=0),
                                           current_korisnik=Depends(get_current_user),
                                           params: Params = Depends()):
    zainteresovani_kupci = await (Stan.outerjoin(Ugovor, Stan.id == Ugovor.stan_id)
                                  .outerjoin(Kupac, Ugovor.kupac_id == Kupac.id)
                                  .select().where(
        and_(Stan.id == stan_id, Stan.status != StatusStana.PRODAT)).gino.load(
        Stan.distinct(Stan.id).load(kupci=Kupac)).all())
    return paginate(zainteresovani_kupci, params)


@app.post("/stan/dodeli_zainteresovanog_kupca", response_model=schemas.UgovorBaseSchema,
          dependencies=[Depends(RoleChecker(["Adrministrator", "Prodavac"]))])
async def create_ugovor(ugovor: schemas.CreateUgovorSchema,
                        current_korisnik=Depends(get_current_user)):
    ugovor_exists = await Ugovor.query.where(
        and_(Ugovor.stan_id == ugovor.stan_id, Ugovor.kupac_id == ugovor.kupac_id)).gino.first()
    if ugovor_exists:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,
                            detail="Ovaj kupac je vec na listi zainteresovanih.")

    stan = await Stan.get(ugovor.stan_id)
    if not stan:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Stan nije pronadjen")

    if ugovor.dogovorena_cena:
        if stan.cena != ugovor.dogovorena_cena:
            ugovor.odobreno = Odobrenje.NE
    else:
        ugovor.dogovorena_cena = stan.cena

    return await Ugovor.create(**ugovor.dict(exclude_unset=True))


# ovo je ruta samo za finansij
@app.get("/stan/odobrenje", response_model=Page[schemas.UgovorOdobrenjeSchema],
         dependencies=[Depends(RoleChecker(["Finansije"]))])
async def get_ugovori(current_korisnik=Depends(get_current_user), params: Params = Depends()):
    ugovor = await (Ugovor.outerjoin(Stan, Ugovor.stan_id == Stan.id)
                    .outerjoin(Kupac, Ugovor.kupac_id == Kupac.id)
                    .select().where(Ugovor.odobreno == Odobrenje.NE).gino
                    .load(Ugovor.distinct(Ugovor.id).load(kupac=Kupac).load(stan=Stan)).all())
    return paginate(ugovor, params)


# ovo je ruta samo za finansije
@app.patch("/stan/{ugovor_id}/ugovor", response_model=schemas.UgovorUpdateReturnSchema,
           dependencies=[Depends(RoleChecker(["Finansije"]))])
async def odobri_ugovor(update_data: schemas.UgovorOdobrenjeUpdateSchema,
                        ugovor_id: int = Path(..., gt=0),
                        current_korisnik=Depends(get_current_user)):
    ugovor = await Ugovor.get(ugovor_id)

    await ugovor.update(**update_data.dict(exclude_unset=True),
                        datum_azuriranja=datetime.now(),
                        odobrio_korisnik=current_korisnik.id).apply()

    updated_ugovor = await (Ugovor.outerjoin(Stan, Ugovor.stan_id == Stan.id)
                            .outerjoin(Kupac, Ugovor.kupac_id == Kupac.id)
                            .outerjoin(Korisnik, Ugovor.odobrio_korisnik == Korisnik.id)
                            .select().where(Ugovor.id == ugovor_id).gino
                            .load(Ugovor.distinct(Ugovor.id)
                                  .load(kupac=Kupac)
                                  .load(stan=Stan)
                                  .load(odobrile_finansije=Korisnik)).first())

    return updated_ugovor


@app.patch("/stan/{stan_id}/rezervacija", response_model=schemas.StanBaseSchema,
           dependencies=[Depends(RoleChecker(["Adrministrator", "Prodavac"]))])
async def rezervisi_stan(update_data: schemas.StanRezervacijaSchema,
                         stan_id: int = Path(..., gt=0),
                         current_korisnik=Depends(get_current_user)):
    stan = await Stan.get(stan_id)
    ugovor = await Ugovor.query.where(
        and_(Ugovor.stan_id == stan_id, Ugovor.kupac_id == update_data.kupac_id)).gino.first()

    if not stan:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Stan nije pronadjen")

    if not ugovor:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,
                            detail="Ne postoji Ugovor, potrebno je kupca prvo dodati "
                                   "na listu zainteresovanih za ovaj stan")

    if ugovor.odobreno == Odobrenje.NE:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,
                            detail="Potrebno je odobrenje od strane Finansija.")

    if stan.status in [StatusStana.PRODAT.value, StatusStana.REZERVISAN]:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,
                            detail=f"Nije moguce rezervisati stan koji je u statusu {stan.status.value}")

    updated_stan = await stan.update(status=StatusStana.REZERVISAN.value,
                                     kupac_id=update_data.kupac_id,
                                     datum_azuriranja=datetime.now()).apply()

    filepath = await generate_ugovor_data(ugovor)

    await ugovor.update(word_doc_path=filepath,
                        datum_azuriranja=datetime.now()).apply()

    # ae kao moze da se loaduje i kupac koji je rezervisao u taj stan...npr pa to da se vrati tako
    # ili da se loaduje ugovor moze svasta.
    return updated_stan._instance


@app.patch("/stan/{stan_id}/dostupan", response_model=schemas.StanBaseSchema,
           dependencies=[Depends(RoleChecker(["Adrministrator", "Prodavac"]))])
async def oslobodi_stan(stan_id: int = Path(..., gt=0),
                        current_korisnik=Depends(get_current_user)):
    stan = await Stan.get(stan_id)
    ugovor = await Ugovor.query.where(
        and_(Ugovor.stan_id == stan_id, Ugovor.kupac_id == stan.kupac_id)).gino.first()

    if not stan:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Stan nije pronadjen")

    if stan.status == StatusStana.PRODAT.value:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,
                            detail="Nije moguce menjati status stana koji je Prodat.")

    updated_stan = await stan.update(status=StatusStana.DOSTUPAN,
                                     kupac_id=None,
                                     datum_azuriranja=datetime.now()).apply()
    await ugovor.update(word_doc_path=None,
                        broj_ugovora=None,
                        datum_azuriranja=datetime.now()).apply()

    return updated_stan._instance


@app.patch("/stan/{stan_id}/prodaja", response_model=schemas.StanBaseSchema,
           dependencies=[Depends(RoleChecker(["Adrministrator", "Prodavac"]))])
async def oslobodi_stan(update_data: schemas.StanProdajaSchema,
                        stan_id: int = Path(..., gt=0),
                        current_korisnik=Depends(get_current_user)):
    stan = await Stan.get(stan_id)
    ugovor = await Ugovor.query.where(
        and_(Ugovor.stan_id == stan_id, Ugovor.kupac_id == stan.kupac_id)).gino.first()

    if not stan:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Stan nije pronadjen")

    if stan.status in [StatusStana.PRODAT.value, StatusStana.DOSTUPAN]:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,
                            detail=f"Nije moguce prodati stan u statusu {stan.status}")

    updated_stan = await stan.update(status=StatusStana.PRODAT,
                                     datum_azuriranja=datetime.now()).apply()
    await ugovor.update(broj_ugovora=update_data.broj_ugovora,
                        datum_azuriranja=datetime.now()).apply()

    return updated_stan._instance


@app.get("/stan/prodati_stanovi/", response_model=Page[schemas.UgovorOdobrenjeSchema],
         status_code=status.HTTP_200_OK,
         dependencies=[Depends(RoleChecker(["Adrministrator", "Prodavac"]))])
async def get_ugovori(current_korisnik=Depends(get_current_user),
                      params: Params = Depends()):
    ugovori = await (Ugovor.outerjoin(Stan, Ugovor.stan_id == Stan.id)
                     .outerjoin(Kupac, Ugovor.kupac_id == Kupac.id)
                     .select().where(
        and_(Stan.status == StatusStana.PRODAT, Ugovor.kupac_id == Stan.kupac_id)).gino
                     .load(Ugovor.distinct(Ugovor.id).load(kupac=Kupac).load(stan=Stan)).all())
    return paginate(ugovori, params)


@app.get("/stan/{stan_id}/ugovor", response_model=dict, status_code=status.HTTP_200_OK)
async def get_word_ugovor(stan_id: int = Path(..., gt=0),
                          current_korisnik=Depends(get_current_user)):
    stan = await Stan.get(stan_id)
    ugovor = await Ugovor.query.where(
        and_(Ugovor.stan_id == stan_id, Ugovor.kupac_id == stan.kupac_id)).gino.first()
    if not ugovor:
        raise HTTPException(status_code=status.HTTP_204_NO_CONTENT,
                            detail="Nije pronadjen ugovor za ovaj stan.")

    return {"url": f"{ugovor.word_doc_path}"}


@app.get("/stan/izvestaj/po_statusu", response_model=dict, status_code=status.HTTP_200_OK,
         dependencies=[Depends(RoleChecker(["Adrministrator", "Prodavac"]))])
async def get_broj_stanova_po_status(datum_od: date = date(2021, 1, 1),
                                     datum_do: date = date.today(),
                                     current_korisnik=Depends(get_current_user)):
    stanovi_po_statusu = dict()

    dostupni = await (
        db.select([db.func.count()])
            .where(and_(Stan.status == StatusStana.DOSTUPAN,
                        Stan.datum_azuriranja.between(datum_od, datum_do))).gino.scalar())

    rezervisani = await (
        db.select([db.func.count()])
            .where(and_(Stan.status == StatusStana.REZERVISAN,
                        Stan.datum_azuriranja.between(datum_od, datum_do))).gino.scalar())

    prodati = await (
        db.select([db.func.count()])
            .where(and_(Stan.status == StatusStana.PRODAT,
                        Stan.datum_azuriranja.between(datum_od, datum_do))).gino.scalar())

    stanovi_po_statusu['Dostupni'] = dostupni
    stanovi_po_statusu['Rezervisani'] = rezervisani
    stanovi_po_statusu['Prodati'] = prodati
    return stanovi_po_statusu


@app.get("/stan/izvestaj/prodati", response_model=dict, status_code=status.HTTP_200_OK,
         dependencies=[Depends(RoleChecker(["Adrministrator", "Prodavac"]))])
async def get_prodati_stanovi_stats(datum_od: date = date(2021, 1, 1),
                                    datum_do: date = date.today(),
                                    current_korisnik=Depends(get_current_user)):
    izvestaj_o_prodaji = dict()

    broj_prodatih_stanova = await (
        db.select([db.func.count()])
            .where(and_(Stan.status == StatusStana.PRODAT,
                        Stan.datum_azuriranja.between(datum_od, datum_do))).gino.scalar())

    suma_cene_prodatih_stanova = await (
        db.select([db.func.sum(Stan.cena)])
            .where(and_(Stan.status == StatusStana.PRODAT,
                        Stan.datum_azuriranja.between(datum_od, datum_do))).gino.scalar())

    suma_dogovorene_cene_prodatih_stanova = (
        await db.select([db.func.sum(Ugovor.dogovorena_cena)])
            .select_from(Stan.outerjoin(Ugovor, Stan.id == Ugovor.stan_id))
            .where(and_(Stan.status == StatusStana.PRODAT,
                        Stan.datum_azuriranja.between(datum_od, datum_do))).gino.scalar()
    )

    izvestaj_o_prodaji['broj_prodatih_stanova'] = broj_prodatih_stanova
    izvestaj_o_prodaji['suma_regularne_cene_stanova'] = suma_cene_prodatih_stanova
    izvestaj_o_prodaji['suma_dogovorene_cene_stanova'] = suma_dogovorene_cene_prodatih_stanova
    izvestaj_o_prodaji[
        'profit_sumarno'] = suma_cene_prodatih_stanova - suma_dogovorene_cene_prodatih_stanova

    return izvestaj_o_prodaji


@app.get("/stan/izvestaj/{kupac_id}/interesovanje",
         response_model=List[schemas.IzvestajKupacSchema], status_code=status.HTTP_200_OK,
         dependencies=[Depends(RoleChecker(["Adrministrator", "Prodavac", "Finansije"]))])
async def get_stanovi_koje_kupac_prati(kupac_id: int = Path(..., gt=0),
                                       current_korisnik=Depends(get_current_user)):
    interesantni_stanovi = await (Ugovor.outerjoin(Stan, Ugovor.stan_id == Stan.id)
                                  .select().where(
        and_(Ugovor.kupac_id == kupac_id, Stan.status != StatusStana.PRODAT)).gino
                                  .load(Ugovor.distinct(Ugovor.id)
                                        .load(stan=Stan)).all())

    return interesantni_stanovi


@app.get("/stan/izvestaj/{kupac_id}/kupljeni_stanovi",
         response_model=List[schemas.IzvestajKupacSchema], status_code=status.HTTP_200_OK,
         dependencies=[Depends(RoleChecker(["Adrministrator", "Prodavac", "Finansije"]))])
async def get_kupljeni_stanovi_for_kupac(kupac_id: int = Path(..., gt=0),
                                         datum_od: date = date(2021, 1, 1),
                                         datum_do: date = date.today(),
                                         current_korisnik=Depends(get_current_user)):
    kupljeni_stanovi = await (Ugovor.outerjoin(Stan, Ugovor.stan_id == Stan.id)
                              .select().where(
        and_(Ugovor.kupac_id == kupac_id, Stan.status == StatusStana.PRODAT,
             Stan.datum_azuriranja.between(datum_od, datum_do))).gino
                              .load(Ugovor.distinct(Ugovor.id)
                                    .load(stan=Stan)).all())

    return kupljeni_stanovi
